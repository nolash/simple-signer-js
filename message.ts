import { Envelope } from './envelope';
import { Contact, KeyContact } from './contact';

class Message {

	envelope:	Envelope
	contact:	Contact

	constructor(envelope:Envelope, contact:Contact) {
		if (!envelope.isSigned()) {
			throw('envelope must be signed');
		}

		this.envelope = envelope;
		this.contact = contact;
	}

	public static fromJSON(s:string): Message {
		let o = JSON.parse(s);
		console.debug('deserializing json', o);
		let envelope = Envelope.fromJSON(JSON.stringify(o.envelope));
		let contact = new Contact(o.contact.commonName, o.contact.metaUrl);
		return new Message(envelope, contact);
	}


	public verify(): boolean {
		const publicKey = this.envelope.recover();
		const keyContact = KeyContact.fromContact(this.contact, publicKey);
		const signature = this.envelope.getSignature();
	       	return keyContact.verify(this.envelope.hash, signature);
	}


	public getContact(): KeyContact {
		let publicKey = this.envelope.recover();
		return KeyContact.fromContact(this.contact, publicKey);
	}


	public getContent(): Uint8Array {
		return this.envelope.content;
	}
}


export { Message };
