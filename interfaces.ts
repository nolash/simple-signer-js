interface KeyHolder {
	publicKey:	Uint8Array
	verify(message:Uint8Array, signature:Signature): boolean
}


interface Signature {
	signature: 	Uint8Array
	recid:		number
};


interface Signer {
	signDigest(digest:Uint8Array): Signature
}


interface Serializable {
	serialize():	 	Uint8Array
	readonly typ:		Uint8Array
}


export { KeyHolder, Signature, Signer, Serializable };
