# simple-signer - sign anything, simply

A signature scheme where self-serializable objects can be signed, while building a chain of signature proofs.

## Installation

`npm install --save simple-signer`

## Example usage

```javascript
let s = require('simple-signer');

// implements interfaces/Serializable
function SerializableObject(data) {
	this.data = data
	this.typ = new Uint8Array([42])
}

SerializableObject.prototype.serialize = function() {
	return new Uint8Array(this.data);
}


// wrap data with envelope and sign envelope with wallet
let zeroHash = new Uint8Array(32);
let aliceMnemonic = "soup high fat equip protect unusual tag helmet detail erupt hover boss glad crew relief narrow message grid swing tone physical inquiry satisfy habit";
let aliceWallet = new s.Wallet(aliceMnemonic);
let contactAlice = new s.Contact('alice', 'http://cryptocouple.com/')
let now = Math.floor(Date.now()/1000);
let serializableObject = new SerializableObject(['f', 'o', 'o']);
let envelopeAlice = new s.Envelope(zeroHash, serializableObject);
envelopeAlice.sign(aliceWallet);


// use message to transmit envelope and contact between devices
let messageAlice = new s.Message(envelopeAlice, contactAlice);
let messageAliceJson = JSON.stringify(messageAlice);


// unpack, verify and extract contents
let messageAliceRecovered = s.Message.fromJSON(messageAliceJson);
let verifyAlice = messageAliceRecovered.verify()
let contactAliceRecovered = messageAliceRecovered.getContact();
```

## Building

`npm run build`

## Running tests

`npm test`
