import { Work, limits } from '../work';
import * as assert from 'assert';


describe('work', () => {
	it('create', function() {
		let fail = false;
		let w = undefined;
		let description = 'and no play ';

		// input checking catches negative weight
		try {
			w = new Work(description, -1, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

		// input checking catches zero weight
		w = undefined;
		try {
			w = new Work(description, 0, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

		// input passes invalid values
		w = new Work(description, 1, limits.timestamp);
		assert.notEqual(w, undefined);

		w = new Work(description, limits.weight, limits.timestamp);
		assert.notEqual(w, undefined);

		// input checking catches too height weigth
		w = undefined;
		try {
			w = new Work(description, limits.weight + 1, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

		// input checking catches too low timestamp
		w = undefined;
		try {
			w = new Work(description, limits.weight, limits.timestamp - 1);
		} catch {
		}
		assert.equal(w, undefined);
		
		// input checking catches too long name
		w = undefined;
		for (; description.length < limits.description + 1; description += description);
		try {
			w = new Work(description, 42, limits.timestamp);
		} catch {
		}
		assert.equal(w, undefined);

	});

	it('json', function() {
		let description = 'and no play ';
		let weight = 42;
		let timestamp = limits.timestamp;
		let wi = new Work(description, weight, timestamp);

		// export to json has valid values
		//let jo = wi.toJson();
		let jo = JSON.stringify(wi);

		// import from json has valid values
		let wo = Work.fromJSON(jo);
		assert.equal(wo.description, description);
		assert.equal(wo.weight, weight);
		assert.equal(wo.timestamp, timestamp);

	});


	it('protobuf', function() {
		let description = 'and no play ';
		let weight = 42;
		let timestamp = limits.timestamp;
		let wi = new Work(description, weight, timestamp);

		//let msgBuf = wi.serialize(wi);
		let msgBuf = wi.serialize();

		let wo = Work.deserialize(msgBuf);

		assert.deepEqual(wo, wi);
	});
});
